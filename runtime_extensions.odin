//+private
package wasm

// Imports and wraps extra functionality provided by different wasn rutimes
/**
 * Functionality matrix:
 *          | wasmer | wasmtime
 * wat2wasm |  yes   |  yes
 */

import "core:c"

when WASM_RUNTIME == "wasmer" {
	when ODIN_OS == .Windows { foreign import wasm "wasmer.dll.lib" }
	else                     { foreign import wasm "system:wasmer" }
} else when WASM_RUNTIME == "wasmtime" {
	when ODIN_OS == .Windows { foreign import wasm "wasmtime.dll.lib" }
	else                     { foreign import wasm "system:wasmtime" }
} else {
	#panic("No WASM_RUNTIME set!")
}

// Wat2Wasm_Result :: enum {}

when WASM_RUNTIME == "wasmer" {
	foreign wasm {
		@(link_name="wat2wasm")
		wasmer_wat2wasm :: proc "c" (wat: ^byte_vec, own_out: ^byte_vec) ---
	}

	wat2wasm :: proc(const_wat: ^byte_vec, own_out: ^byte_vec) -> bool {
		wasmer_wat2wasm(const_wat, own_out)
		if own_out.size == 0 {
			byte_vec_delete(own_out)
			return false
		}
		return true
	}
} else when WASM_RUNTIME == "wasmtime" {
	wasmtime_error :: struct {}
	foreign wasm {
		// Note that the parameter names have been corrected to follow wasm.h semantics
		wasmtime_error_delete :: proc(error: ^wasmtime_error) ---
		// wasmtime_error_message :: proc(const_error: ^wasmtime_error, own_out: ^message) ---
		wasmtime_wat2wasm :: proc(wat: cstring, wat_len: c.size_t, own_out: ^byte_vec) -> ^wasmtime_error ---
	}

	wat2wasm :: proc(const_wat: ^byte_vec, own_out: ^byte_vec) -> bool {
		wat_string := cstring(const_wat.data[:const_wat.size])
		err := wasmtime_wat2wasm(wat_string, len(wat_string), own_out)

		if err != nil {
			// own_out will not have been filled, according to the wasmtime docs
			wasmtime_error_delete(err) // TODO[TS]: We could also get the error message here
			return false
		}
		return true
	}
}
