# TODO: The helpers should be generated from the main file as we already know more about the types there.

def write_line(file, line):
	file.write(line)
	file.write("\n")

def opt_ptr(name):
	if name == "byte" or name == "val": return name
	return f"^{name}"

def main():
	base_own_types = {"config","engine","store","frame"}
	base_vec_types = {"byte","val","frame","extern"}
	base_type_types = {"valtype","functype","globaltype","tabletype","memorytype","externtype","importtype","exporttype"}
	base_ref_base_types = {"ref"}
	base_ref_types = {"trap","func","global","table","memory","extern","instance"}
	base_shareable_ref_types = {"module"}

	shareable_ref_types = base_shareable_ref_types
	ref_types = base_ref_types | shareable_ref_types
	ref_base_types = base_ref_base_types | ref_types
	type_types = base_type_types
	vec_types = base_vec_types | type_types
	own_types = base_own_types | type_types | ref_base_types | ref_types | shareable_ref_types

	f = open("wasm_odin_helpers.odin", "wt")

	# HEADER
	write_line(f, "package wasm")
	write_line(f, "")
	write_line(f, 'import "core:c"')
	write_line(f, "")

	# VEC HELPERS
	write_line(f, "// vec helpers")
	write_line(f, "")

	# slice_to_vec
	for type in vec_types:
		space = " " * (15 - len(type))
		write_line(f, f'slice_to_{type}_vec {space}:: #force_inline proc "contextless" (slice: []{opt_ptr(type)}) -> {type}_vec {{ return {{ data = raw_data(slice), size = c.size_t(len(slice)) }} }}')

	f.write("slice_to_vec :: proc{")
	for type in vec_types:
		f.write(f'slice_to_{type}_vec,')
	f.write("}\n\n")

	# vec_to_slice
	for type in vec_types:
		space = " " * (15 - len(type))
		write_line(f, f'{type}_vec_to_slice {space}:: #force_inline proc "contextless" (vec: ^{type}_vec) -> []{opt_ptr(type)} {{ return vec.data[:vec.size] }}')

	f.write("vec_to_slice :: proc{")
	for type in vec_types:
		f.write(f'{type}_vec_to_slice,')
	f.write("}\n\n")

	# vec_create_empty
	for type in vec_types:
		space = " " * (15 - len(type))
		write_line(f, f'{type}_vec_create_empty {space}:: #force_inline proc "contextless" () -> {type}_vec {{ vec: {type}_vec; {type}_vec_new_empty(&vec); return vec }}')
	f.write("\n")

	# vec_create
	for type in vec_types:
		space = " " * (15 - len(type))
		write_line(f, f'{type}_vec_create_from_size {space}:: #force_inline proc "contextless" (size: c.size_t) -> {type}_vec {{ vec: {type}_vec; {type}_vec_new_uninitialized(&vec, size); return vec }}')
	f.write("\n")

	# vec_create
	for type in vec_types:
		space = " " * (15 - len(type))
		write_line(f, f'{type}_vec_create_from_slice {space}:: #force_inline proc "contextless" (slice: []{opt_ptr(type)}) -> {type}_vec {{ vec: {type}_vec; {type}_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }}')

	f.write("vec_create_from_slice :: proc{")
	for type in vec_types:
		f.write(f'{type}_vec_create_from_slice,')
	f.write("}\n\n")

	# vec_delete
	f.write("vec_delete :: proc{")
	for type in vec_types:
		f.write(f'{type}_vec_delete,')
	f.write("}\n")

	# vec_copy
	f.write("vec_copy :: proc{")
	for type in vec_types:
		f.write(f'{type}_vec_copy,')
	f.write("}\n")

	f.close()

if __name__ == "__main__":
	main()
