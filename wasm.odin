package wasm

import "core:c"

WASM_RUNTIME :: #config(WASM_RUNTIME, "none")

when WASM_RUNTIME == "wasmer" {
	when ODIN_OS == .Windows { foreign import wasm "wasmer.dll.lib" }
	else                     { foreign import wasm "system:wasmer" }
} else when WASM_RUNTIME == "wasmtime" {
	when ODIN_OS == .Windows { foreign import wasm "wasmtime.dll.lib" }
	else                     { foreign import wasm "system:wasmtime" }
} else {
	#panic("No WASM_RUNTIME set!")
}

byte_vec :: struct {
	size: c.size_t,
	data: [^]byte,
}

@(link_prefix="wasm_")
foreign wasm {
	byte_vec_new_empty         :: proc(own_out: ^byte_vec) ---
	byte_vec_new_uninitialized :: proc(own_out: ^byte_vec, size: c.size_t) ---
	byte_vec_new               :: proc(own_out: ^byte_vec, size: c.size_t, from: [^]byte) ---
	byte_vec_copy              :: proc(own_out: ^byte_vec, const_to_copy: ^byte_vec) ---
	byte_vec_delete            :: proc(own_vec: ^byte_vec) ---
}

name                       :: byte_vec
name_new                   :: byte_vec_new
name_new_empty             :: byte_vec_new_empty
name_new_new_uninitialized :: byte_vec_new_uninitialized
name_copy                  :: byte_vec_copy
name_delete                :: byte_vec_delete

config :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	config_delete :: proc(own_config: ^config) ---

	config_new :: proc() -> (own_config: ^config) ---
}

engine :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	engine_delete :: proc(own_engine: ^engine) ---

	engine_new             :: proc() -> (own_engine: ^engine) ---
	engine_new_with_config :: proc(own_config: ^config) -> (own_engine: ^engine) ---
}

store :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	store_delete :: proc(own_store: ^store) ---

	store_new :: proc(engine: ^engine) -> (own_store: ^store) ---
}


mutability :: enum u8 {
	CONST,
	VAR,
}

limits :: struct {
	min: u32,
	max: u32,
}

limits_max_default :: u32(0xffffffff)


valkind :: enum u8 {
	I32,
	I64,
	F32,
	F64,
	ANYREF = 128,
	FUNCREF,
}

valtype :: struct {}

valtype_vec :: struct {
	size: c.size_t,
	data: [^]^valtype,
}

@(link_prefix="wasm_")
foreign wasm {
	valtype_delete :: proc(own_valtype: ^valtype) ---

	valtype_vec_new_empty         :: proc(own_out: ^valtype_vec) ---
	valtype_vec_new_uninitialized :: proc(own_out: ^valtype_vec, size: c.size_t) ---
	valtype_vec_new               :: proc(own_out: ^valtype_vec, size: c.size_t, from: [^]^valtype) ---
	valtype_vec_copy              :: proc(own_out: ^valtype_vec, const_to_copy: ^valtype_vec) ---
	valtype_vec_delete            :: proc(own_vec: ^valtype_vec) ---

	valtype_copy :: proc(valtype_to_copy: ^valtype) -> (own_valtype: ^valtype) ---

	valtype_new :: proc(kind: valkind) -> (own_valtype: ^valtype) ---
	valtype_kind :: proc(const_valtype: ^valtype) -> valkind ---
}


valkind_is_num :: #force_inline proc "contextless" (k: valkind) -> bool {
	return k < .ANYREF
}
valkind_is_ref :: #force_inline proc "contextless" (k: valkind) -> bool {
	return k >= .ANYREF
}

valtype_is_num :: #force_inline proc "contextless" (const_valtype: ^valtype) -> bool {
	return valkind_is_num(valtype_kind(const_valtype))
}
valtype_is_ref :: #force_inline proc "contextless" (const_valtype: ^valtype) -> bool {
	return valkind_is_ref(valtype_kind(const_valtype))
}

functype :: struct {}

functype_vec :: struct {
	size: c.size_t,
	data: [^]^functype,
}

@(link_prefix="wasm_")
foreign wasm {
	functype_delete :: proc(own_functype: ^functype) ---

	functype_vec_new_empty         :: proc(own_out: ^functype_vec) ---
	functype_vec_new_uninitialized :: proc(own_out: ^functype_vec, size: c.size_t) ---
	functype_vec_new               :: proc(own_out: ^functype_vec, size: c.size_t, from: [^]^functype) ---
	functype_vec_copy              :: proc(own_out: ^functype_vec, const_to_copy: ^functype_vec) ---
	functype_vec_delete            :: proc(own_vec: ^functype_vec) ---

	functype_copy :: proc(functype_to_copy: ^functype) -> (own_functype: ^functype) ---

	functype_new     :: proc(own_params: ^valtype_vec, own_results: ^valtype_vec) -> (own_functype: ^functype) ---
	functype_params  :: proc(const_valtype: ^valtype) -> (const_valtype_vec: ^valtype_vec) ---
	functype_results :: proc(const_valtype: ^valtype) -> (const_valtype_vec: ^valtype_vec) ---
}

globaltype :: struct {}

globaltype_vec :: struct {
	size: c.size_t,
	data: [^]^globaltype,
}

@(link_prefix="wasm_")
foreign wasm {
	globaltype_delete :: proc(own_globaltype: ^globaltype) ---

	globaltype_vec_new_empty         :: proc(own_out: ^globaltype_vec) ---
	globaltype_vec_new_uninitialized :: proc(own_out: ^globaltype_vec, size: c.size_t) ---
	globaltype_vec_new               :: proc(own_out: ^globaltype_vec, size: c.size_t, from: [^]^globaltype) ---
	globaltype_vec_copy              :: proc(own_out: ^globaltype_vec, const_to_copy: ^globaltype_vec) ---
	globaltype_vec_delete            :: proc(own_vec: ^globaltype_vec) ---

	globaltype_copy :: proc(globaltype_to_copy: ^globaltype) -> (own_globaltype: ^globaltype) ---

	globaltype_new        :: proc(own_valtype: ^valtype, mutability: mutability) -> (own_globaltype: ^globaltype) ---
	globaltype_content    :: proc(const_globaltype: ^globaltype) -> (const_valtype: ^valtype) ---
	globaltype_mutability :: proc(const_globaltype: ^globaltype) -> mutability ---
}

tabletype :: struct {}

tabletype_vec :: struct {
	size: c.size_t,
	data: [^]^tabletype,
}

@(link_prefix="wasm_")
foreign wasm {
	tabletype_delete :: proc(own_tabletype: ^tabletype) ---

	tabletype_vec_new_empty         :: proc(own_out: ^tabletype_vec) ---
	tabletype_vec_new_uninitialized :: proc(own_out: ^tabletype_vec, size: c.size_t) ---
	tabletype_vec_new               :: proc(own_out: ^tabletype_vec, size: c.size_t, from: [^]^tabletype) ---
	tabletype_vec_copy              :: proc(own_out: ^tabletype_vec, const_to_copy: ^tabletype_vec) ---
	tabletype_vec_delete            :: proc(own_vec: ^tabletype_vec) ---

	tabletype_copy :: proc(tabletype_to_copy: ^tabletype) -> (own_tabletype: ^tabletype) ---

	tabletype_new     :: proc(own_valtype: ^valtype, const_limits: ^limits) -> (own_tabletype: ^tabletype) ---
	tabletype_element :: proc(const_tabletype: ^tabletype) -> (const_valtype: ^valtype) ---
	tabletype_limits  :: proc(const_tabletype: ^tabletype) -> (const_limits: ^limits) ---
}

memorytype :: struct {}

memorytype_vec :: struct {
	size: c.size_t,
	data: [^]^memorytype,
}

@(link_prefix="wasm_")
foreign wasm {
	memorytype_delete :: proc(own_memorytype: ^memorytype) ---

	memorytype_vec_new_empty         :: proc(own_out: ^memorytype_vec) ---
	memorytype_vec_new_uninitialized :: proc(own_out: ^memorytype_vec, size: c.size_t) ---
	memorytype_vec_new               :: proc(own_out: ^memorytype_vec, size: c.size_t, from: [^]^memorytype) ---
	memorytype_vec_copy              :: proc(own_out: ^memorytype_vec, const_to_copy: ^memorytype_vec) ---
	memorytype_vec_delete            :: proc(own_vec: ^memorytype_vec) ---

	memorytype_copy :: proc(memorytype_to_copy: ^memorytype) -> (own_memorytype: ^memorytype) ---

	memorytype_new    :: proc(const_limits: ^limits) -> (own_memorytype: ^memorytype) ---
	memorytype_limits :: proc(const_memorytype: ^memorytype) -> (const_limits: ^limits) ---
}

externtype :: struct {}

externtype_vec :: struct {
	size: c.size_t,
	data: [^]^externtype,
}

@(link_prefix="wasm_")
foreign wasm {
	externtype_delete :: proc(own_externtype: ^externtype) ---

	externtype_vec_new_empty         :: proc(own_out: ^externtype_vec) ---
	externtype_vec_new_uninitialized :: proc(own_out: ^externtype_vec, size: c.size_t) ---
	externtype_vec_new               :: proc(own_out: ^externtype_vec, size: c.size_t, from: [^]^externtype) ---
	externtype_vec_copy              :: proc(own_out: ^externtype_vec, const_to_copy: ^externtype_vec) ---
	externtype_vec_delete            :: proc(own_vec: ^externtype_vec) ---

	externtype_copy :: proc(externtype_to_copy: ^externtype) -> (own_externtype: ^externtype) ---
}


externkind :: enum u8 {
	FUNC,
	GLOBAL,
	TABLE,
	MEMORY,
}

@(link_prefix="wasm_")
foreign wasm {
	externtype_kind :: proc(const_externtype: ^externtype) -> externkind ---

	functype_as_externtype   :: proc(functype: ^functype) -> ^externtype ---
	globaltype_as_externtype :: proc(globaltype: ^globaltype) -> ^externtype ---
	tabletype_as_externtype  :: proc(tabletype: ^tabletype) -> ^externtype ---
	memorytype_as_externtype :: proc(memorytype: ^memorytype) -> ^externtype ---

	externtype_as_functype   :: proc(externtype: ^externtype) -> ^functype ---
	externtype_as_globaltype :: proc(externtype: ^externtype) -> ^globaltype ---
	externtype_as_tabletype  :: proc(externtype: ^externtype) -> ^tabletype ---
	externtype_as_memorytype :: proc(externtype: ^externtype) -> ^memorytype ---

	functype_as_externtype_const   :: proc(const_functype: ^functype) -> (const_externtype: ^externtype) ---
	globaltype_as_externtype_const :: proc(const_globaltype: ^globaltype) -> (const_externtype: ^externtype) ---
	tabletype_as_externtype_const  :: proc(const_tabletype: ^tabletype) -> (const_externtype: ^externtype) ---
	memorytype_as_externtype_const :: proc(const_memorytype: ^memorytype) -> (const_externtype: ^externtype) ---

	externtype_as_functype_const   :: proc(const_externtype: ^externtype) -> (const_functype: ^functype) ---
	externtype_as_globaltype_const :: proc(const_externtype: ^externtype) -> (const_globaltype: ^globaltype) ---
	externtype_as_tabletype_const  :: proc(const_externtype: ^externtype) -> (const_tabletype: ^tabletype) ---
	externtype_as_memorytype_const :: proc(const_externtype: ^externtype) -> (const_memorytype: ^memorytype) ---
}

importtype :: struct {}

importtype_vec :: struct {
	size: c.size_t,
	data: [^]^importtype,
}

@(link_prefix="wasm_")
foreign wasm {
	importtype_delete :: proc(own_importtype: ^importtype) ---

	importtype_vec_new_empty         :: proc(own_out: ^importtype_vec) ---
	importtype_vec_new_uninitialized :: proc(own_out: ^importtype_vec, size: c.size_t) ---
	importtype_vec_new               :: proc(own_out: ^importtype_vec, size: c.size_t, from: [^]^importtype) ---
	importtype_vec_copy              :: proc(own_out: ^importtype_vec, const_to_copy: ^importtype_vec) ---
	importtype_vec_delete            :: proc(own_vec: ^importtype_vec) ---

	importtype_copy :: proc(importtype_to_copy: ^importtype) -> (own_importtype: ^importtype) ---

	importtype_new    :: proc(own_module: ^name, own_name: ^name, own_externtype: ^externtype) -> (own_importtype: ^importtype) ---
	importtype_module :: proc(const_importtype: ^importtype) -> (const_name: ^name) ---
	importtype_name   :: proc(const_importtype: ^importtype) -> (const_name: ^name) ---
	importtype_type   :: proc(const_importtype: ^importtype) -> (const_externtype: ^externtype) ---
}

exporttype :: struct {}

exporttype_vec :: struct {
	size: c.size_t,
	data: [^]^exporttype,
}

@(link_prefix="wasm_")
foreign wasm {
	exporttype_delete :: proc(own_exporttype: ^exporttype) ---

	exporttype_vec_new_empty         :: proc(own_out: ^exporttype_vec) ---
	exporttype_vec_new_uninitialized :: proc(own_out: ^exporttype_vec, size: c.size_t) ---
	exporttype_vec_new               :: proc(own_out: ^exporttype_vec, size: c.size_t, from: [^]^exporttype) ---
	exporttype_vec_copy              :: proc(own_out: ^exporttype_vec, const_to_copy: ^exporttype_vec) ---
	exporttype_vec_delete            :: proc(own_vec: ^exporttype_vec) ---

	exporttype_copy :: proc(exporttype_to_copy: ^exporttype) -> (own_exporttype: ^exporttype) ---

	exporttype_new  :: proc(own_name: ^name, own_externtype: ^externtype) -> (own_exporttype: ^exporttype) ---
	exporttype_name :: proc(const_exporttype: ^exporttype) -> (const_name: ^name) ---
	exporttype_type :: proc(const_exporttype: ^exporttype) -> (const_externtype: ^externtype) ---
}


val :: struct {
	kind: valkind,
	of: struct #raw_union {
		_i32: i32,
		_i64: i64,
		_f32: f32,
		_f64: f64,
		ref: ^ref,
	},
}

@(link_prefix="wasm_")
foreign wasm {
	val_delete :: proc(own_v: ^val) ---
	val_copy   :: proc(own_out: ^val, const_v: ^val) ---
}

val_vec :: struct {
	size: c.size_t,
	data: [^]val,
}

@(link_prefix="wasm_")
foreign wasm {
	val_vec_new_empty         :: proc(own_out: ^val_vec) ---
	val_vec_new_uninitialized :: proc(own_out: ^val_vec, size: c.size_t) ---
	val_vec_new               :: proc(own_out: ^val_vec, size: c.size_t, from: [^]val) ---
	val_vec_copy              :: proc(own_out: ^val_vec, const_to_copy: ^val_vec) ---
	val_vec_delete            :: proc(own_vec: ^val_vec) ---
}

ref :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	ref_delete :: proc(own_ref: ^ref) ---

	ref_same                         :: proc(const_ref_1: ^ref, const_ref_2: ^ref) -> bool ---
	ref_copy                         :: proc(const_ref: ^ref) -> (own_ref: ^ref) ---
	ref_get_host_info                :: proc(const_ref: ^ref) -> rawptr ---
	ref_set_host_info                :: proc(ref: ^ref, data: rawptr) ---
	ref_set_host_info_with_finalizer :: proc(ref: ^ref, data: rawptr, finalizer: proc(data: rawptr)) ---
}

frame :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	frame_delete :: proc(own_frame: ^frame) ---

	frame_copy          :: proc(const_frame: ^frame) -> (own_frame: ^frame) ---
	frame_instance      :: proc(const_frame: ^frame) -> ^instance ---
	frame_func_index    :: proc(const_frame: ^frame) -> u32 ---
	frame_func_offset   :: proc(const_frame: ^frame) -> c.size_t ---
	frame_module_offset :: proc(const_frame: ^frame) -> c.size_t ---
}

frame_vec :: struct {
	size: c.size_t,
	data: [^]^frame,
}

@(link_prefix="wasm_")
foreign wasm {
	frame_vec_new_empty         :: proc(own_out: ^frame_vec) ---
	frame_vec_new_uninitialized :: proc(own_out: ^frame_vec, size: c.size_t) ---
	frame_vec_new               :: proc(own_out: ^frame_vec, size: c.size_t, from: [^]^frame) ---
	frame_vec_copy              :: proc(own_out: ^frame_vec, const_to_copy: ^frame_vec) ---
	frame_vec_delete            :: proc(own_vec: ^frame_vec) ---
}

message :: name

trap :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	trap_delete :: proc(own_trap: ^trap) ---

	trap_same                         :: proc(const_trap_1: ^trap, const_trap_2: ^trap) -> bool ---
	trap_copy                         :: proc(const_trap: ^trap) -> (own_trap: ^trap) ---
	trap_get_host_info                :: proc(const_trap: ^trap) -> rawptr ---
	trap_set_host_info                :: proc(trap: ^trap, data: rawptr) ---
	trap_set_host_info_with_finalizer :: proc(trap: ^trap, data: rawptr, finalizer: proc(data: rawptr)) ---

	trap_as_ref :: proc(trap: ^trap) -> ^ref ---
	ref_as_trap :: proc(ref: ^ref) -> ^trap ---
	trap_as_ref_const :: proc(const_trap: ^trap) -> (const_ref: ^ref) ---
	ref_as_trap_const :: proc(const_ref: ^ref) -> (const_name: ^trap) ---

	trap_new     :: proc(store: ^store, const_message: ^message) -> (own_trap: ^trap) ---
	trap_message :: proc(const_trap: ^trap, own_out: ^message) ---
	trap_origin  :: proc(const_trap: ^trap) -> (own_frame: ^frame) ---
	trap_trace   :: proc(const_trap: ^trap, own_out: ^frame) ---
}

module :: struct {}

shared_module :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	module_delete :: proc(own_module: ^module) ---
	shared_module_delete :: proc(own_shared_module: ^shared_module) ---

	module_same                         :: proc(const_module_1: ^module, const_module_2: ^module) -> bool ---
	module_copy                         :: proc(const_module: ^module) -> (own_module: ^module) ---
	module_get_host_info                :: proc(const_module: ^module) -> rawptr ---
	module_set_host_info                :: proc(module: ^module, data: rawptr) ---
	module_set_host_info_with_finalizer :: proc(module: ^module, data: rawptr, finalizer: proc(data: rawptr)) ---

	module_as_ref :: proc(module: ^module) -> ^ref ---
	ref_as_module :: proc(ref: ^ref) -> ^module ---
	module_as_ref_const :: proc(const_module: ^module) -> (const_ref: ^ref) ---
	ref_as_module_const :: proc(const_ref: ^ref) -> (const_name: ^module) ---

	module_share :: proc(const_module: ^module) -> (own_shared_module: ^shared_module) ---
	module_obtain :: proc(store: ^store, const_shared_module: ^shared_module) -> (own_module: ^module) ---

	module_new         :: proc(store: ^store, const_binary: ^byte_vec) -> (own_module: ^module) ---
	module_validate    :: proc(store: ^store, const_binary: ^byte_vec) -> bool ---
	module_imports     :: proc(const_module: ^module, own_out: ^importtype_vec) ---
	module_exports     :: proc(const_module: ^module, own_out: ^exporttype_vec) ---
	module_serialize   :: proc(const_module: ^module, own_out: ^byte_vec) ---
	module_deserialize :: proc(store: ^store, const_byte_vec: ^byte_vec) -> (own_module: ^module) ---
}

func_callback          :: #type proc "c" (const_args: ^val_vec, own_results: ^val_vec) -> (own_trap: ^trap)
func_callback_with_env :: #type proc "c" (env: rawptr, const_args: ^val_vec, results: ^val_vec) -> (own_trap: ^trap)

func :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	func_delete :: proc(own_func: ^func) ---

	func_same                         :: proc(const_func_1: ^func, const_func_2: ^func) -> bool ---
	func_copy                         :: proc(const_func: ^func) -> (own_func: ^func) ---
	func_get_host_info                :: proc(const_func: ^func) -> rawptr ---
	func_set_host_info                :: proc(func: ^func, data: rawptr) ---
	func_set_host_info_with_finalizer :: proc(func: ^func, data: rawptr, finalizer: proc(data: rawptr)) ---

	func_as_ref :: proc(func: ^func) -> ^ref ---
	ref_as_func :: proc(ref: ^ref) -> ^func ---
	func_as_ref_const :: proc(const_func: ^func) -> (const_ref: ^ref) ---
	ref_as_func_const :: proc(const_ref: ^ref) -> (const_name: ^func) ---

	func_new :: proc(store: ^store, const_functype: ^functype, func_callback: func_callback) -> (own_func: ^func) ---
	func_new_with_env :: proc(
		store: ^store,
		const_functype: ^functype,
		func_callback_with_env: func_callback_with_env,
		env: rawptr,
		finalizer: proc "c" (env: rawptr),
	) -> (own_func: ^func) ---
	func_type         :: proc(const_func: ^func) -> (own_functype: ^functype) ---
	func_param_arity  :: proc(const_func: ^func) -> c.size_t ---
	func_result_arity :: proc(const_func: ^func) -> c.size_t ---
	func_call         :: proc(const_func: ^func, const_args: ^val_vec, results: ^val_vec) -> (own_trap: ^trap) ---
}

global :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	global_delete :: proc(own_global: ^global) ---

	global_same                         :: proc(const_global_1: ^global, const_global_2: ^global) -> bool ---
	global_copy                         :: proc(const_global: ^global) -> (own_global: ^global) ---
	global_get_host_info                :: proc(const_global: ^global) -> rawptr ---
	global_set_host_info                :: proc(global: ^global, data: rawptr) ---
	global_set_host_info_with_finalizer :: proc(global: ^global, data: rawptr, finalizer: proc(data: rawptr)) ---

	global_as_ref :: proc(global: ^global) -> ^ref ---
	ref_as_global :: proc(ref: ^ref) -> ^global ---
	global_as_ref_const :: proc(const_global: ^global) -> (const_ref: ^ref) ---
	ref_as_global_const :: proc(const_ref: ^ref) -> (const_name: ^global) ---

	global_new  :: proc(store: ^store, const_globaltype: ^globaltype, const_val: ^val) -> (own_global: ^global) ---
	global_type :: proc(const_global: ^global) -> (own_globaltype: ^globaltype) ---
	global_get  :: proc(const_global: ^global, own_out: ^val) ---
	global_set  :: proc(global: ^global, const_val: ^val) ---
}

table_size_t :: u32

table :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	table_delete :: proc(own_table: ^table) ---

	table_same                         :: proc(const_table_1: ^table, const_table_2: ^table) -> bool ---
	table_copy                         :: proc(const_table: ^table) -> (own_table: ^table) ---
	table_get_host_info                :: proc(const_table: ^table) -> rawptr ---
	table_set_host_info                :: proc(table: ^table, data: rawptr) ---
	table_set_host_info_with_finalizer :: proc(table: ^table, data: rawptr, finalizer: proc(data: rawptr)) ---

	table_as_ref :: proc(table: ^table) -> ^ref ---
	ref_as_table :: proc(ref: ^ref) -> ^table ---
	table_as_ref_const :: proc(const_table: ^table) -> (const_ref: ^ref) ---
	ref_as_table_const :: proc(const_ref: ^ref) -> (const_name: ^table) ---

	table_new  :: proc(store: ^store, const_tabletype: ^tabletype, init: ^ref) -> (own_table: ^table) ---
	table_type :: proc(const_table: ^table) -> (own_tabletype: ^tabletype) ---
	table_get  :: proc(const_table: ^table, index: table_size_t) -> (own_ref: ^ref) ---
	table_set  :: proc(table: ^table, index: table_size_t, ref: ^ref) -> bool ---
	table_size :: proc(const_table: ^table) -> table_size_t ---
	table_grob :: proc(table: ^table, delta: table_size_t, init: ^ref) -> bool ---
}

memory_pages :: u32
MEMORY_PAGE_SIZE :: c.size_t(0x10000)

memory :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	memory_delete :: proc(own_memory: ^memory) ---

	memory_same                         :: proc(const_memory_1: ^memory, const_memory_2: ^memory) -> bool ---
	memory_copy                         :: proc(const_memory: ^memory) -> (own_memory: ^memory) ---
	memory_get_host_info                :: proc(const_memory: ^memory) -> rawptr ---
	memory_set_host_info                :: proc(memory: ^memory, data: rawptr) ---
	memory_set_host_info_with_finalizer :: proc(memory: ^memory, data: rawptr, finalizer: proc(data: rawptr)) ---

	memory_as_ref :: proc(memory: ^memory) -> ^ref ---
	ref_as_memory :: proc(ref: ^ref) -> ^memory ---
	memory_as_ref_const :: proc(const_memory: ^memory) -> (const_ref: ^ref) ---
	ref_as_memory_const :: proc(const_ref: ^ref) -> (const_name: ^memory) ---

	memory_new       :: proc(store: ^store, const_memorytype: ^memorytype) -> (own_memory: ^memory) ---
	memory_type      :: proc(const_memory: ^memory) -> (own_memorytype: ^memorytype) ---
	memory_data      :: proc(memory: ^memory) -> [^]byte ---
	memory_data_size :: proc(const_memory: ^memory) -> c.size_t ---
	memory_size      :: proc(const_memory: ^memory) -> memory_pages ---
	memory_grow      :: proc(memory: ^memory, delta: memory_pages) -> bool ---
}

extern :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	extern_delete :: proc(own_extern: ^extern) ---

	extern_same                         :: proc(const_extern_1: ^extern, const_extern_2: ^extern) -> bool ---
	extern_copy                         :: proc(const_extern: ^extern) -> (own_extern: ^extern) ---
	extern_get_host_info                :: proc(const_extern: ^extern) -> rawptr ---
	extern_set_host_info                :: proc(extern: ^extern, data: rawptr) ---
	extern_set_host_info_with_finalizer :: proc(extern: ^extern, data: rawptr, finalizer: proc(data: rawptr)) ---

	extern_as_ref :: proc(extern: ^extern) -> ^ref ---
	ref_as_extern :: proc(ref: ^ref) -> ^extern ---
	extern_as_ref_const :: proc(const_extern: ^extern) -> (const_ref: ^ref) ---
	ref_as_extern_const :: proc(const_ref: ^ref) -> (const_name: ^extern) ---
}

@(link_prefix="wasm_")
foreign wasm {
	extern_kind :: proc(const_extern: ^extern) -> externkind ---
	extern_type :: proc(const_extern: ^extern) -> (own_externtype: ^externtype) ---

	func_as_extern   :: proc(func: ^func) -> ^extern ---
	global_as_extern :: proc(global: ^global) -> ^extern ---
	table_as_extern  :: proc(table: ^table) -> ^extern ---
	memory_as_extern :: proc(memory: ^memory) -> ^extern ---

	extern_as_func   :: proc(extern: ^extern) -> ^func ---
	extern_as_global :: proc(extern: ^extern) -> ^global ---
	extern_as_table  :: proc(extern: ^extern) -> ^table ---
	extern_as_memory :: proc(extern: ^extern) -> ^memory ---

	func_as_extern_const   :: proc(const_func: ^func) -> (const_extern: ^extern) ---
	global_as_extern_const :: proc(const_global: ^global) -> (const_extern: ^extern) ---
	table_as_extern_const  :: proc(const_table: ^table) -> (const_extern: ^extern) ---
	memory_as_extern_const :: proc(const_memory: ^memory) -> (const_extern: ^extern) ---

	extern_as_func_const   :: proc(const_extern: ^extern) -> (const_func: ^func) ---
	extern_as_global_const :: proc(const_extern: ^extern) -> (const_global: ^global) ---
	extern_as_table_const  :: proc(const_extern: ^extern) -> (const_table: ^table) ---
	extern_as_memory_const :: proc(const_extern: ^extern) -> (const_memory: ^memory) ---
}

extern_vec :: struct {
	size: c.size_t,
	data: [^]^extern,
}

@(link_prefix="wasm_")
foreign wasm {
	extern_vec_new_empty         :: proc(own_out: ^extern_vec) ---
	extern_vec_new_uninitialized :: proc(own_out: ^extern_vec, size: c.size_t) ---
	extern_vec_new               :: proc(own_out: ^extern_vec, size: c.size_t, from: [^]^extern) ---
	extern_vec_copy              :: proc(own_out: ^extern_vec, const_to_copy: ^extern_vec) ---
	extern_vec_delete            :: proc(own_vec: ^extern_vec) ---
}

instance :: struct {}

@(link_prefix="wasm_")
foreign wasm {
	instance_delete :: proc(own_instance: ^instance) ---

	instance_same                         :: proc(const_instance_1: ^instance, const_instance_2: ^instance) -> bool ---
	instance_copy                         :: proc(const_instance: ^instance) -> (own_instance: ^instance) ---
	instance_get_host_info                :: proc(const_instance: ^instance) -> rawptr ---
	instance_set_host_info                :: proc(instance: ^instance, data: rawptr) ---
	instance_set_host_info_with_finalizer :: proc(instance: ^instance, data: rawptr, finalizer: proc(data: rawptr)) ---

	instance_as_ref :: proc(instance: ^instance) -> ^ref ---
	ref_as_instance :: proc(ref: ^ref) -> ^instance ---
	instance_as_ref_const :: proc(const_instance: ^instance) -> (const_ref: ^ref) ---
	ref_as_instance_const :: proc(const_ref: ^ref) -> (const_name: ^instance) ---

	instance_new     :: proc(store: ^store, const_module: ^module, const_imports: ^extern_vec, own_trap: ^^trap) -> (own_instance: ^instance) ---
	instance_exports :: proc(const_instance: ^instance, own_out: ^extern_vec) ---
}

