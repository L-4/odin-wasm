## Odin WASM

This project wraps the [wasm.h header](https://github.com/WebAssembly/wasm-c-api/blob/b6dd1fb658a282c64b029867845bc50ae59e1497/include/wasm.h). This header represents the interface to a webassembly virtual machine, allowing to call in/out, grab references to WASM object etc.

Note that this is generated from a specific version of the header (permalinked for all links in this file), but the header was last changed in November of 2020, so it can be considered quite stable.

Alongside this wrapper, you will need to compile a WASM runtime. I have personally used [Wasmer](https://github.com/wasmerio/wasmer), however there are [https://github.com/appcypher/awesome-wasm-runtimes](plenty more runtimes).

If you choose to use a different runtime than wasmer, then you'll have to go into `build.py` and change `WASM_IMPORT_WINDOWS` and `WASM_IMPORT_OTHER` and regenerate the wrapper to import the correct library.

## Semantic parameter names
The parameter (and sometimes return) names in wasm.h have some semantic meaning. Specifically, `const *` as is common in C, as well as `own`.

There is no special meaning to `const *` beyond its meaning in C, but `own` generally indicates ownership semantics. Examples:
 - A return type of `own foo_t` means that you are in charge of freeing the value.
 - Passing an object to a function with the signature `foo(own foo_t* v)` means that `foo` takes ownership of the value.
 - Parameters that look like `(own foo_t* out)` instead work the other way around, filling `out` with an object which you now have ownership of.

Please read [the documentation in wasm.h](https://github.com/WebAssembly/wasm-c-api/blob/b6dd1fb658a282c64b029867845bc50ae59e1497/include/wasm.h#L46) for further info.

In odin, all `own` parameters/return values have `own_` prepended. Similarly, all `const*` parameters have `const_` prepended.

Alternatively, by setting the `USE_BY_PTR` option to true in `build.py`, `const*` parameters will instead be passed with `#by_ptr v: foo`. This internally uses a pointer, but follows Odin's convention of having immutable parameters passed transparently by either value or const reference.

## Helpers

In `wasm_odin_helpers.odin` there are a few additional helpers, for now only dealing with copying between wasm.h vectors and slices, and allocating new slices.
