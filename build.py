# If this is true, values passed by `const *` will be passed with #by_ptr instead of ^
USE_BY_PTR = False
# Wasm implementation shared library to try to load
WASM_IMPORT_WINDOWS = "wasmer.dll.lib"
WASM_IMPORT_OTHER = "system:wasmer"

wasm_runtime_infos = [
	{
		"name": "wasmer",
		"import_windows": "wasmer.dll.lib",
		"import_other": "system:wasmer"
	},
	{
		"name": "wasmtime",
		"import_windows": "wasmtime.dll.lib",
		"import_other": "system:wasmtime",
	}
]

wasm_file = open("wasm.odin", "wt")

def write(str=''):
	global wasm_file
	if in_foreign and str != '':
		wasm_file.writelines(['\t', str, '\n'])
	else:
		wasm_file.writelines([str, '\n'])

def write_no_newline(str):
	global wasm_file
	wasm_file.writelines([str]) # TODO: In foreign?

def write_header():
	write('package wasm')
	write()
	write('import "core:c"')
	write()
	write('WASM_RUNTIME :: #config(WASM_RUNTIME, "none")')
	write()

	for runtime_idx in range(len(wasm_runtime_infos)):
		runtime_info = wasm_runtime_infos[runtime_idx]
		write(f'when WASM_RUNTIME == "{runtime_info["name"]}" {{')
		write(f'	when ODIN_OS == .Windows {{ foreign import wasm "{runtime_info["import_windows"]}" }}')
		write(f'	else                     {{ foreign import wasm "{runtime_info["import_other"]}" }}')
		if runtime_idx < (len(wasm_runtime_infos) - 1):
			write_no_newline(f'}} else ')

	write(f'}} else {{')
	write(f'	#panic("No WASM_RUNTIME set!")')
	write(f'}}')

	write()

def const(name, type=None):
	if type == None:
		type = name
	global USE_BY_PTR

	if USE_BY_PTR:
		return f'#by_ptr {name}: {type}'
	else:
		return f'const_{name}: ^{type}'

# def own(name, type=None): # @todo
in_foreign = False

def start_foreign():
	global in_foreign
	write(f'@(link_prefix="wasm_")')
	write(f'foreign wasm {{')
	in_foreign = True

def end_foreign():
	global in_foreign
	in_foreign = False
	write(f'}}')
	write()

def wasm_declare_own_types(name):
	write(f'{name} :: struct {{}}')
	write()

def wasm_declare_own_impl(name):
	write(f'{name}_delete :: proc(own_{name}: ^{name}) ---')

def wasm_declare_own(name, close_foreign=True):
	wasm_declare_own_types(name)
	start_foreign()
	wasm_declare_own_impl(name)
	if close_foreign: end_foreign()
	else: write()

def wasm_declare_vec_types(name, is_ptr):
	p = "^" if is_ptr else ""
	write(f'{name}_vec :: struct {{')
	write(f'	size: c.size_t,')
	write(f'	data: [^]{p}{name},')
	write(f'}}')
	write()

def wasm_declare_vec_impl(name, is_ptr):
	p = "^" if is_ptr else ""
	write(f'{name}_vec_new_empty         :: proc(own_out: ^{name}_vec) ---')
	write(f'{name}_vec_new_uninitialized :: proc(own_out: ^{name}_vec, size: c.size_t) ---')
	write(f'{name}_vec_new               :: proc(own_out: ^{name}_vec, size: c.size_t, from: [^]{p}{name}) ---')
	write(f'{name}_vec_copy              :: proc(own_out: ^{name}_vec, {const("to_copy", f"{name}_vec")}) ---')
	write(f'{name}_vec_delete            :: proc(own_vec: ^{name}_vec) ---')

def wasm_declare_vec(name, is_ptr=False, close_foreign=True):
	wasm_declare_vec_types(name, is_ptr)
	start_foreign()
	wasm_declare_vec_impl(name, is_ptr)
	if close_foreign: end_foreign()
	else: write()

def wasm_declare_type_impl(name):
	write(f'{name}_copy :: proc({name}_to_copy: ^{name}) -> (own_{name}: ^{name}) ---')

def wasm_declare_type(name, close_foreign=True):
	wasm_declare_own_types(name)
	wasm_declare_vec_types(name, True)
	start_foreign()
	wasm_declare_own_impl(name)
	write()
	wasm_declare_vec_impl(name, True)
	write()
	wasm_declare_type_impl(name)
	if close_foreign: end_foreign()
	else: write()

def wasm_declare_ref_base_impl(name):
	write(f'{name}_same                         :: proc({const(f"{name}_1", name)}, {const(f"{name}_2", name)}) -> bool ---')
	write(f'{name}_copy                         :: proc({const(name)}) -> (own_{name}: ^{name}) ---')
	write(f'{name}_get_host_info                :: proc({const(name)}) -> rawptr ---')
	write(f'{name}_set_host_info                :: proc({name}: ^{name}, data: rawptr) ---')
	write(f'{name}_set_host_info_with_finalizer :: proc({name}: ^{name}, data: rawptr, finalizer: proc(data: rawptr)) ---')

def wasm_declare_ref_base(name, close_foreign=True):
	wasm_declare_own_types(name)
	start_foreign()
	wasm_declare_own_impl(name)
	write()
	wasm_declare_ref_base_impl(name)
	if close_foreign: end_foreign()
	else: write()

def wasm_declare_ref_impl(name):
	write(f'{name}_as_ref :: proc({name}: ^{name}) -> ^ref ---')
	write(f'ref_as_{name} :: proc(ref: ^ref) -> ^{name} ---')
	write(f'{name}_as_ref_const :: proc({const(name)}) -> (const_ref: ^ref) ---')
	write(f'ref_as_{name}_const :: proc({const("ref")}) -> (const_name: ^{name}) ---')

def wasm_declare_ref(name, close_foreign=True):
	wasm_declare_own_types(name)
	start_foreign()
	wasm_declare_own_impl(name)
	write()
	wasm_declare_ref_base_impl(name)
	write()
	wasm_declare_ref_impl(name)
	if close_foreign: end_foreign()
	else: write()

def wasm_declare_shareable_ref_impl(name):
	write(f'{name}_share :: proc({const(name)}) -> (own_shared_{name}: ^shared_{name}) ---')
	write(f'{name}_obtain :: proc(store: ^store, {const(f"shared_{name}")}) -> (own_{name}: ^{name}) ---')

def wasm_declare_sharable_ref(name, close_foreign=True):
	wasm_declare_own_types(name)
	wasm_declare_own_types(f"shared_{name}")
	start_foreign()
	wasm_declare_own_impl(name)
	wasm_declare_own_impl(f"shared_{name}")
	write()
	wasm_declare_ref_base_impl(name)
	write()
	wasm_declare_ref_impl(name)
	write()
	wasm_declare_shareable_ref_impl(name)
	if close_foreign: end_foreign()
	else: write()

def main():
	global f

	write_header()

	wasm_declare_vec("byte")

	write('name                       :: byte_vec')
	write('name_new                   :: byte_vec_new')
	write('name_new_empty             :: byte_vec_new_empty')
	write('name_new_new_uninitialized :: byte_vec_new_uninitialized')
	write('name_copy                  :: byte_vec_copy')
	write('name_delete                :: byte_vec_delete')
	write()

	wasm_declare_own("config", False)
	write('config_new :: proc() -> (own_config: ^config) ---')
	end_foreign()

	wasm_declare_own("engine", False)
	write('engine_new             :: proc() -> (own_engine: ^engine) ---')
	write('engine_new_with_config :: proc(own_config: ^config) -> (own_engine: ^engine) ---')
	end_foreign()

	wasm_declare_own("store", False)
	write('store_new :: proc(engine: ^engine) -> (own_store: ^store) ---')
	end_foreign()

	write("""
mutability :: enum u8 {
	CONST,
	VAR,
}

limits :: struct {
	min: u32,
	max: u32,
}

limits_max_default :: u32(0xffffffff)
""")

	write("""
valkind :: enum u8 {
	I32,
	I64,
	F32,
	F64,
	ANYREF = 128,
	FUNCREF,
}
""")

	wasm_declare_type("valtype", False)
	write(f'valtype_new :: proc(kind: valkind) -> (own_valtype: ^valtype) ---')
	write(f'valtype_kind :: proc({const("valtype", "valtype")}) -> valkind ---')
	end_foreign()

	write(f"""
valkind_is_num :: #force_inline proc "contextless" (k: valkind) -> bool {{
	return k < .ANYREF
}}
valkind_is_ref :: #force_inline proc "contextless" (k: valkind) -> bool {{
	return k >= .ANYREF
}}

valtype_is_num :: #force_inline proc "contextless" (const_valtype: ^valtype) -> bool {{
	return valkind_is_num(valtype_kind(const_valtype))
}}
valtype_is_ref :: #force_inline proc "contextless" (const_valtype: ^valtype) -> bool {{
	return valkind_is_ref(valtype_kind(const_valtype))
}}
""")

	wasm_declare_type("functype", False)
	write(f'functype_new     :: proc(own_params: ^valtype_vec, own_results: ^valtype_vec) -> (own_functype: ^functype) ---')
	write(f'functype_params  :: proc({const("valtype")}) -> (const_valtype_vec: ^valtype_vec) ---')
	write(f'functype_results :: proc({const("valtype")}) -> (const_valtype_vec: ^valtype_vec) ---')
	end_foreign()

	wasm_declare_type("globaltype", False)
	write(f'globaltype_new        :: proc(own_valtype: ^valtype, mutability: mutability) -> (own_globaltype: ^globaltype) ---')
	write(f'globaltype_content    :: proc({const("globaltype")}) -> (const_valtype: ^valtype) ---')
	write(f'globaltype_mutability :: proc({const("globaltype")}) -> mutability ---')
	end_foreign()

	wasm_declare_type("tabletype", False)
	write(f'tabletype_new     :: proc(own_valtype: ^valtype, const_limits: ^limits) -> (own_tabletype: ^tabletype) ---')
	write(f'tabletype_element :: proc({const("tabletype")}) -> (const_valtype: ^valtype) ---')
	write(f'tabletype_limits  :: proc({const("tabletype")}) -> (const_limits: ^limits) ---')
	end_foreign()

	wasm_declare_type("memorytype", False)
	write(f'memorytype_new    :: proc({const("limits")}) -> (own_memorytype: ^memorytype) ---')
	write(f'memorytype_limits :: proc({const("memorytype")}) -> (const_limits: ^limits) ---')
	end_foreign()

	wasm_declare_type("externtype")

	write("""
externkind :: enum u8 {
	FUNC,
	GLOBAL,
	TABLE,
	MEMORY,
}
""")

	start_foreign()
	write(f'externtype_kind :: proc({const("externtype")}) -> externkind ---')
	write()
	write(f'functype_as_externtype   :: proc(functype: ^functype) -> ^externtype ---')
	write(f'globaltype_as_externtype :: proc(globaltype: ^globaltype) -> ^externtype ---')
	write(f'tabletype_as_externtype  :: proc(tabletype: ^tabletype) -> ^externtype ---')
	write(f'memorytype_as_externtype :: proc(memorytype: ^memorytype) -> ^externtype ---')
	write()
	write(f'externtype_as_functype   :: proc(externtype: ^externtype) -> ^functype ---')
	write(f'externtype_as_globaltype :: proc(externtype: ^externtype) -> ^globaltype ---')
	write(f'externtype_as_tabletype  :: proc(externtype: ^externtype) -> ^tabletype ---')
	write(f'externtype_as_memorytype :: proc(externtype: ^externtype) -> ^memorytype ---')
	write()
	write(f'functype_as_externtype_const   :: proc({const("functype")}) -> (const_externtype: ^externtype) ---')
	write(f'globaltype_as_externtype_const :: proc({const("globaltype")}) -> (const_externtype: ^externtype) ---')
	write(f'tabletype_as_externtype_const  :: proc({const("tabletype")}) -> (const_externtype: ^externtype) ---')
	write(f'memorytype_as_externtype_const :: proc({const("memorytype")}) -> (const_externtype: ^externtype) ---')
	write()
	write(f'externtype_as_functype_const   :: proc({const("externtype")}) -> (const_functype: ^functype) ---')
	write(f'externtype_as_globaltype_const :: proc({const("externtype")}) -> (const_globaltype: ^globaltype) ---')
	write(f'externtype_as_tabletype_const  :: proc({const("externtype")}) -> (const_tabletype: ^tabletype) ---')
	write(f'externtype_as_memorytype_const :: proc({const("externtype")}) -> (const_memorytype: ^memorytype) ---')
	end_foreign()

	wasm_declare_type("importtype", False)
	write(f'importtype_new    :: proc(own_module: ^name, own_name: ^name, own_externtype: ^externtype) -> (own_importtype: ^importtype) ---')
	write(f'importtype_module :: proc({const("importtype")}) -> (const_name: ^name) ---')
	write(f'importtype_name   :: proc({const("importtype")}) -> (const_name: ^name) ---')
	write(f'importtype_type   :: proc({const("importtype")}) -> (const_externtype: ^externtype) ---')
	end_foreign()

	wasm_declare_type("exporttype", False)
	write(f'exporttype_new  :: proc(own_name: ^name, own_externtype: ^externtype) -> (own_exporttype: ^exporttype) ---')
	write(f'exporttype_name :: proc({const("exporttype")}) -> (const_name: ^name) ---')
	write(f'exporttype_type :: proc({const("exporttype")}) -> (const_externtype: ^externtype) ---')
	end_foreign()

	write("""
val :: struct {
	kind: valkind,
	of: struct #raw_union {
		_i32: i32,
		_i64: i64,
		_f32: f32,
		_f64: f64,
		ref: ^ref,
	},
}
""")

	start_foreign()
	write(f'val_delete :: proc(own_v: ^val) ---')
	write(f'val_copy   :: proc(own_out: ^val, {const("v", "val")}) ---')
	end_foreign()

	wasm_declare_vec("val")

	wasm_declare_ref_base("ref")

	wasm_declare_own("frame", False)
	write(f'frame_copy          :: proc({const("frame")}) -> (own_frame: ^frame) ---')
	write(f'frame_instance      :: proc({const("frame")}) -> ^instance ---')
	write(f'frame_func_index    :: proc({const("frame")}) -> u32 ---')
	write(f'frame_func_offset   :: proc({const("frame")}) -> c.size_t ---')
	write(f'frame_module_offset :: proc({const("frame")}) -> c.size_t ---')
	end_foreign()

	wasm_declare_vec("frame", True)

	write("message :: name")
	write()

	wasm_declare_ref("trap", False)
	write(f'trap_new     :: proc(store: ^store, {const("message")}) -> (own_trap: ^trap) ---')
	write(f'trap_message :: proc({const("trap")}, own_out: ^message) ---')
	write(f'trap_origin  :: proc({const("trap")}) -> (own_frame: ^frame) ---')
	write(f'trap_trace   :: proc({const("trap")}, own_out: ^frame) ---')
	end_foreign()

	wasm_declare_sharable_ref("module", False)
	write(f'module_new         :: proc(store: ^store, {const("binary", "byte_vec")}) -> (own_module: ^module) ---')
	write(f'module_validate    :: proc(store: ^store, {const("binary", "byte_vec")}) -> bool ---')
	write(f'module_imports     :: proc({const("module")}, own_out: ^importtype_vec) ---')
	write(f'module_exports     :: proc({const("module")}, own_out: ^exporttype_vec) ---')
	write(f'module_serialize   :: proc({const("module")}, own_out: ^byte_vec) ---')
	write(f'module_deserialize :: proc(store: ^store, {const("byte_vec")}) -> (own_module: ^module) ---')
	end_foreign()

	write(f'func_callback          :: #type proc "c" ({const("args", "val_vec")}, own_results: ^val_vec) -> (own_trap: ^trap)')
	# TODO[TS]: Why does this version of the callback not use own_results?
	write(f'func_callback_with_env :: #type proc "c" (env: rawptr, {const("args", "val_vec")}, results: ^val_vec) -> (own_trap: ^trap)')
	write()

	wasm_declare_ref("func", False)
	write(f'func_new :: proc(store: ^store, {const("functype")}, func_callback: func_callback) -> (own_func: ^func) ---')
	write(f'func_new_with_env :: proc(')
	write(f'	store: ^store,')
	write(f'	{const("functype")},')
	write(f'	func_callback_with_env: func_callback_with_env,')
	write(f'	env: rawptr,')
	write(f'	finalizer: proc "c" (env: rawptr),')
	write(f') -> (own_func: ^func) ---')
	write(f'func_type         :: proc({const("func")}) -> (own_functype: ^functype) ---')
	write(f'func_param_arity  :: proc({const("func")}) -> c.size_t ---')
	write(f'func_result_arity :: proc({const("func")}) -> c.size_t ---')
	write(f'func_call         :: proc({const("func")}, {const("args", "val_vec")}, results: ^val_vec) -> (own_trap: ^trap) ---')
	end_foreign()

	wasm_declare_ref("global", False)
	write(f'global_new  :: proc(store: ^store, {const("globaltype")}, const_val: ^val) -> (own_global: ^global) ---')
	write(f'global_type :: proc({const("global")}) -> (own_globaltype: ^globaltype) ---')
	write(f'global_get  :: proc({const("global")}, own_out: ^val) ---')
	write(f'global_set  :: proc(global: ^global, const_val: ^val) ---')
	end_foreign()

	# Clashes with wasm.table_size()
	write('table_size_t :: u32')
	write()

	wasm_declare_ref("table", False)
	write(f'table_new  :: proc(store: ^store, {const("tabletype")}, init: ^ref) -> (own_table: ^table) ---')
	write(f'table_type :: proc({const("table")}) -> (own_tabletype: ^tabletype) ---')
	write(f'table_get  :: proc({const("table")}, index: table_size_t) -> (own_ref: ^ref) ---')
	write(f'table_set  :: proc(table: ^table, index: table_size_t, ref: ^ref) -> bool ---')
	write(f'table_size :: proc({const("table")}) -> table_size_t ---')
	write(f'table_grob :: proc(table: ^table, delta: table_size_t, init: ^ref) -> bool ---')
	end_foreign()

	write('memory_pages :: u32')
	write('MEMORY_PAGE_SIZE :: c.size_t(0x10000)')
	write()

	wasm_declare_ref("memory", False)
	write(f'memory_new       :: proc(store: ^store, {const("memorytype")}) -> (own_memory: ^memory) ---')
	write(f'memory_type      :: proc({const("memory")}) -> (own_memorytype: ^memorytype) ---')
	write(f'memory_data      :: proc(memory: ^memory) -> [^]byte ---')
	write(f'memory_data_size :: proc({const("memory")}) -> c.size_t ---')
	write(f'memory_size      :: proc({const("memory")}) -> memory_pages ---')
	write(f'memory_grow      :: proc(memory: ^memory, delta: memory_pages) -> bool ---')
	end_foreign()

	wasm_declare_ref("extern")

	start_foreign()
	write(f'extern_kind :: proc({const("extern")}) -> externkind ---')
	write(f'extern_type :: proc({const("extern")}) -> (own_externtype: ^externtype) ---')
	write()
	write(f'func_as_extern   :: proc(func: ^func) -> ^extern ---')
	write(f'global_as_extern :: proc(global: ^global) -> ^extern ---')
	write(f'table_as_extern  :: proc(table: ^table) -> ^extern ---')
	write(f'memory_as_extern :: proc(memory: ^memory) -> ^extern ---')
	write()
	write(f'extern_as_func   :: proc(extern: ^extern) -> ^func ---')
	write(f'extern_as_global :: proc(extern: ^extern) -> ^global ---')
	write(f'extern_as_table  :: proc(extern: ^extern) -> ^table ---')
	write(f'extern_as_memory :: proc(extern: ^extern) -> ^memory ---')
	write()
	write(f'func_as_extern_const   :: proc({const("func")}) -> (const_extern: ^extern) ---')
	write(f'global_as_extern_const :: proc({const("global")}) -> (const_extern: ^extern) ---')
	write(f'table_as_extern_const  :: proc({const("table")}) -> (const_extern: ^extern) ---')
	write(f'memory_as_extern_const :: proc({const("memory")}) -> (const_extern: ^extern) ---')
	write()
	write(f'extern_as_func_const   :: proc({const("extern")}) -> (const_func: ^func) ---')
	write(f'extern_as_global_const :: proc({const("extern")}) -> (const_global: ^global) ---')
	write(f'extern_as_table_const  :: proc({const("extern")}) -> (const_table: ^table) ---')
	write(f'extern_as_memory_const :: proc({const("extern")}) -> (const_memory: ^memory) ---')
	end_foreign()

	wasm_declare_vec("extern", True)

	wasm_declare_ref("instance", False)
	write(f'instance_new     :: proc(store: ^store, {const("module")}, {const("imports", "extern_vec")}, own_trap: ^^trap) -> (own_instance: ^instance) ---')
	write(f'instance_exports :: proc({const("instance")}, own_out: ^extern_vec) ---')
	end_foreign()

	wasm_file.close()

if __name__ == "__main__":
	main()
