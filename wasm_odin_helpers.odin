package wasm

import "core:c"

// vec helpers

slice_to_functype_vec        :: #force_inline proc "contextless" (slice: []^functype) -> functype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_extern_vec          :: #force_inline proc "contextless" (slice: []^extern) -> extern_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_frame_vec           :: #force_inline proc "contextless" (slice: []^frame) -> frame_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_val_vec             :: #force_inline proc "contextless" (slice: []val) -> val_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_byte_vec            :: #force_inline proc "contextless" (slice: []byte) -> byte_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_tabletype_vec       :: #force_inline proc "contextless" (slice: []^tabletype) -> tabletype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_importtype_vec      :: #force_inline proc "contextless" (slice: []^importtype) -> importtype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_valtype_vec         :: #force_inline proc "contextless" (slice: []^valtype) -> valtype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_globaltype_vec      :: #force_inline proc "contextless" (slice: []^globaltype) -> globaltype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_externtype_vec      :: #force_inline proc "contextless" (slice: []^externtype) -> externtype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_exporttype_vec      :: #force_inline proc "contextless" (slice: []^exporttype) -> exporttype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_memorytype_vec      :: #force_inline proc "contextless" (slice: []^memorytype) -> memorytype_vec { return { data = raw_data(slice), size = c.size_t(len(slice)) } }
slice_to_vec :: proc{slice_to_functype_vec,slice_to_extern_vec,slice_to_frame_vec,slice_to_val_vec,slice_to_byte_vec,slice_to_tabletype_vec,slice_to_importtype_vec,slice_to_valtype_vec,slice_to_globaltype_vec,slice_to_externtype_vec,slice_to_exporttype_vec,slice_to_memorytype_vec,}

functype_vec_to_slice        :: #force_inline proc "contextless" (vec: ^functype_vec) -> []^functype { return vec.data[:vec.size] }
extern_vec_to_slice          :: #force_inline proc "contextless" (vec: ^extern_vec) -> []^extern { return vec.data[:vec.size] }
frame_vec_to_slice           :: #force_inline proc "contextless" (vec: ^frame_vec) -> []^frame { return vec.data[:vec.size] }
val_vec_to_slice             :: #force_inline proc "contextless" (vec: ^val_vec) -> []val { return vec.data[:vec.size] }
byte_vec_to_slice            :: #force_inline proc "contextless" (vec: ^byte_vec) -> []byte { return vec.data[:vec.size] }
tabletype_vec_to_slice       :: #force_inline proc "contextless" (vec: ^tabletype_vec) -> []^tabletype { return vec.data[:vec.size] }
importtype_vec_to_slice      :: #force_inline proc "contextless" (vec: ^importtype_vec) -> []^importtype { return vec.data[:vec.size] }
valtype_vec_to_slice         :: #force_inline proc "contextless" (vec: ^valtype_vec) -> []^valtype { return vec.data[:vec.size] }
globaltype_vec_to_slice      :: #force_inline proc "contextless" (vec: ^globaltype_vec) -> []^globaltype { return vec.data[:vec.size] }
externtype_vec_to_slice      :: #force_inline proc "contextless" (vec: ^externtype_vec) -> []^externtype { return vec.data[:vec.size] }
exporttype_vec_to_slice      :: #force_inline proc "contextless" (vec: ^exporttype_vec) -> []^exporttype { return vec.data[:vec.size] }
memorytype_vec_to_slice      :: #force_inline proc "contextless" (vec: ^memorytype_vec) -> []^memorytype { return vec.data[:vec.size] }
vec_to_slice :: proc{functype_vec_to_slice,extern_vec_to_slice,frame_vec_to_slice,val_vec_to_slice,byte_vec_to_slice,tabletype_vec_to_slice,importtype_vec_to_slice,valtype_vec_to_slice,globaltype_vec_to_slice,externtype_vec_to_slice,exporttype_vec_to_slice,memorytype_vec_to_slice,}

functype_vec_create_empty        :: #force_inline proc "contextless" () -> functype_vec { vec: functype_vec; functype_vec_new_empty(&vec); return vec }
extern_vec_create_empty          :: #force_inline proc "contextless" () -> extern_vec { vec: extern_vec; extern_vec_new_empty(&vec); return vec }
frame_vec_create_empty           :: #force_inline proc "contextless" () -> frame_vec { vec: frame_vec; frame_vec_new_empty(&vec); return vec }
val_vec_create_empty             :: #force_inline proc "contextless" () -> val_vec { vec: val_vec; val_vec_new_empty(&vec); return vec }
byte_vec_create_empty            :: #force_inline proc "contextless" () -> byte_vec { vec: byte_vec; byte_vec_new_empty(&vec); return vec }
tabletype_vec_create_empty       :: #force_inline proc "contextless" () -> tabletype_vec { vec: tabletype_vec; tabletype_vec_new_empty(&vec); return vec }
importtype_vec_create_empty      :: #force_inline proc "contextless" () -> importtype_vec { vec: importtype_vec; importtype_vec_new_empty(&vec); return vec }
valtype_vec_create_empty         :: #force_inline proc "contextless" () -> valtype_vec { vec: valtype_vec; valtype_vec_new_empty(&vec); return vec }
globaltype_vec_create_empty      :: #force_inline proc "contextless" () -> globaltype_vec { vec: globaltype_vec; globaltype_vec_new_empty(&vec); return vec }
externtype_vec_create_empty      :: #force_inline proc "contextless" () -> externtype_vec { vec: externtype_vec; externtype_vec_new_empty(&vec); return vec }
exporttype_vec_create_empty      :: #force_inline proc "contextless" () -> exporttype_vec { vec: exporttype_vec; exporttype_vec_new_empty(&vec); return vec }
memorytype_vec_create_empty      :: #force_inline proc "contextless" () -> memorytype_vec { vec: memorytype_vec; memorytype_vec_new_empty(&vec); return vec }

functype_vec_create_from_size        :: #force_inline proc "contextless" (size: c.size_t) -> functype_vec { vec: functype_vec; functype_vec_new_uninitialized(&vec, size); return vec }
extern_vec_create_from_size          :: #force_inline proc "contextless" (size: c.size_t) -> extern_vec { vec: extern_vec; extern_vec_new_uninitialized(&vec, size); return vec }
frame_vec_create_from_size           :: #force_inline proc "contextless" (size: c.size_t) -> frame_vec { vec: frame_vec; frame_vec_new_uninitialized(&vec, size); return vec }
val_vec_create_from_size             :: #force_inline proc "contextless" (size: c.size_t) -> val_vec { vec: val_vec; val_vec_new_uninitialized(&vec, size); return vec }
byte_vec_create_from_size            :: #force_inline proc "contextless" (size: c.size_t) -> byte_vec { vec: byte_vec; byte_vec_new_uninitialized(&vec, size); return vec }
tabletype_vec_create_from_size       :: #force_inline proc "contextless" (size: c.size_t) -> tabletype_vec { vec: tabletype_vec; tabletype_vec_new_uninitialized(&vec, size); return vec }
importtype_vec_create_from_size      :: #force_inline proc "contextless" (size: c.size_t) -> importtype_vec { vec: importtype_vec; importtype_vec_new_uninitialized(&vec, size); return vec }
valtype_vec_create_from_size         :: #force_inline proc "contextless" (size: c.size_t) -> valtype_vec { vec: valtype_vec; valtype_vec_new_uninitialized(&vec, size); return vec }
globaltype_vec_create_from_size      :: #force_inline proc "contextless" (size: c.size_t) -> globaltype_vec { vec: globaltype_vec; globaltype_vec_new_uninitialized(&vec, size); return vec }
externtype_vec_create_from_size      :: #force_inline proc "contextless" (size: c.size_t) -> externtype_vec { vec: externtype_vec; externtype_vec_new_uninitialized(&vec, size); return vec }
exporttype_vec_create_from_size      :: #force_inline proc "contextless" (size: c.size_t) -> exporttype_vec { vec: exporttype_vec; exporttype_vec_new_uninitialized(&vec, size); return vec }
memorytype_vec_create_from_size      :: #force_inline proc "contextless" (size: c.size_t) -> memorytype_vec { vec: memorytype_vec; memorytype_vec_new_uninitialized(&vec, size); return vec }

functype_vec_create_from_slice        :: #force_inline proc "contextless" (slice: []^functype) -> functype_vec { vec: functype_vec; functype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
extern_vec_create_from_slice          :: #force_inline proc "contextless" (slice: []^extern) -> extern_vec { vec: extern_vec; extern_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
frame_vec_create_from_slice           :: #force_inline proc "contextless" (slice: []^frame) -> frame_vec { vec: frame_vec; frame_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
val_vec_create_from_slice             :: #force_inline proc "contextless" (slice: []val) -> val_vec { vec: val_vec; val_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
byte_vec_create_from_slice            :: #force_inline proc "contextless" (slice: []byte) -> byte_vec { vec: byte_vec; byte_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
tabletype_vec_create_from_slice       :: #force_inline proc "contextless" (slice: []^tabletype) -> tabletype_vec { vec: tabletype_vec; tabletype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
importtype_vec_create_from_slice      :: #force_inline proc "contextless" (slice: []^importtype) -> importtype_vec { vec: importtype_vec; importtype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
valtype_vec_create_from_slice         :: #force_inline proc "contextless" (slice: []^valtype) -> valtype_vec { vec: valtype_vec; valtype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
globaltype_vec_create_from_slice      :: #force_inline proc "contextless" (slice: []^globaltype) -> globaltype_vec { vec: globaltype_vec; globaltype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
externtype_vec_create_from_slice      :: #force_inline proc "contextless" (slice: []^externtype) -> externtype_vec { vec: externtype_vec; externtype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
exporttype_vec_create_from_slice      :: #force_inline proc "contextless" (slice: []^exporttype) -> exporttype_vec { vec: exporttype_vec; exporttype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
memorytype_vec_create_from_slice      :: #force_inline proc "contextless" (slice: []^memorytype) -> memorytype_vec { vec: memorytype_vec; memorytype_vec_new(&vec, c.size_t(len(slice)), raw_data(slice)); return vec }
vec_create_from_slice :: proc{functype_vec_create_from_slice,extern_vec_create_from_slice,frame_vec_create_from_slice,val_vec_create_from_slice,byte_vec_create_from_slice,tabletype_vec_create_from_slice,importtype_vec_create_from_slice,valtype_vec_create_from_slice,globaltype_vec_create_from_slice,externtype_vec_create_from_slice,exporttype_vec_create_from_slice,memorytype_vec_create_from_slice,}

vec_delete :: proc{functype_vec_delete,extern_vec_delete,frame_vec_delete,val_vec_delete,byte_vec_delete,tabletype_vec_delete,importtype_vec_delete,valtype_vec_delete,globaltype_vec_delete,externtype_vec_delete,exporttype_vec_delete,memorytype_vec_delete,}
vec_copy :: proc{functype_vec_copy,extern_vec_copy,frame_vec_copy,val_vec_copy,byte_vec_copy,tabletype_vec_copy,importtype_vec_copy,valtype_vec_copy,globaltype_vec_copy,externtype_vec_copy,exporttype_vec_copy,memorytype_vec_copy,}
