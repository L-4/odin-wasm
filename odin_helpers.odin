package wasm

import "core:runtime"

byte_vec_to_string :: #force_inline proc "contextless" (vec: ^byte_vec) -> string {
	return transmute(string)runtime.Raw_String {
		data = vec.data,
		len = cast(int)vec.size,
	}
}

string_to_byte_vec :: #force_inline proc "contextless" (str: string) -> byte_vec {
	return byte_vec {
		data = raw_data(str),
		size = len(str),
	}
}
